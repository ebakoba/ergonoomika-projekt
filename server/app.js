var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var diskspace = require('diskspace');
var ip = require('ip');
var sensor = require('node-dht-sensor');
var json2csv = require('json2csv');
var sys = require('sys')
var exec = require('child_process').exec;
var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'root',
  database : 'ergonomics'
});
var SerialPort = require('serialport');
var port = new SerialPort("/dev/ttyUSB0", {baudRate: 9600}, function(err){
    if(err)console.log(err);
});
var port2 = new SerialPort('/dev/ttyUSB1', {baudRate: 9600}, function(err){
    if(err) console.log(err);
    
});

arduinoData = [];
sensorsSettings = [];
currentValues = {};
firstData = false;
wifi = true;
frequency = 10;
timer = null;

port.on('data', function(data){
    arduinoHandler(data);
});
port2.on('data', function(data) {
    arduinoHandler(data);
});

arduinoHandler = (data) => {
    data = data.toString('utf8');
    if(data.split(',').length>2){
        handleSensorsArduino(data);
    } else {
        handlePeopleCounterArduino(data);
    }
}

handleSensorsArduino = (data) => {
    if(!firstData){
        firstData = true;
    }
    arduinoData = data.split(',');
    io.emit('carbonDioxideVoltage', parseFloat(arduinoData[2]));
}

handlePeopleCounterArduino = (data) => {
    console.log("inimeste arv", data);
    connection.query('UPDATE settings SET peopleNumber= ?', [data], function(error, results, fields) {});
}

app.use(function (req, res, next) {

    var raspberryOrigin = 'http://' + ip.address();
    var allowedOrigins = ['http://airsensor111', raspberryOrigin, 'http://localhost:3000', 'http://192.168.10.1'];
    var origin = req.headers.origin;
    if(allowedOrigins.indexOf(origin) > -1){
        res.setHeader('Access-Control-Allow-Origin', origin);
    }

    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);

    next();
});

app.get('/memoryUsage', function(req, res) {
    diskspace.check('/', function(err, total, free, status){
        if(err) return res.sendStatus(500);

        var used =  Math.round((100 - (free/total)*100) * 100) / 100
        return res.send({used: used});
    });
});

app.get('/getCurrentValues', function(req, res){
    return res.send({currentValues:currentValues});
});

app.get('/getWifi', function(req, res){
    return res.send({wifi:wifi});
});

app.get('/switchWifi', function(req, res){
    wifi = req.query.wifi === 'true';
    connection.query('UPDATE settings SET wifi= ?', [wifi], function(error, results, fields) {});

    switchWifi(wifi === 'true', function(){
        io.emit('wifiStatus', wifi);
        return res.sendStatus(200);
    });
});

app.get('/getFrequency', function(req, res){
    return res.send({frequency: parseInt(frequency)});
});

app.get('/changeFrequency', function(req, res){
    frequency = req.query.frequency;
    clearTimeout(timer);
    repeat();
    io.emit('frequencyStatus', parseInt(frequency));
    connection.query('UPDATE settings SET frequency= ?', [frequency], function(error, results, fields) {});
    return res.sendStatus(200);
});

app.get('/getSensorsSettings', function(req, res){
    return res.send({sensorsSettings:sensorsSettings});
});

app.get('/changeSensorsSettings', function(req, res){
    var sensor = req.query.sensor;
    var firstParameter = parseFloat(req.query.firstParameter);
    var secondParameter = parseFloat(req.query.secondParameter);

    for(var i =0; i < sensorsSettings.length; i++){
        if(sensorsSettings[i].name === sensor){
            if(sensorsSettings[i].slope) {
                sensorsSettings[i].slope = firstParameter;
                sensorsSettings[i].intercept = secondParameter;
            }
            if(sensorsSettings[i].lowerReaction) {
                sensorsSettings[i].lowerReaction = firstParameter;
                sensorsSettings[i].higherReaction = secondParameter;
            }
        }
    }
    io.emit('sensorsSettings', sensorsSettings);
    connection.query('UPDATE sensors_settings SET firstParameter=?, secondParameter=? WHERE name=?', [firstParameter, secondParameter, sensor],function(error, results, fields){});
    return res.sendStatus(200);
});

app.get('/getDataTimes', function(req, res){
    connection.query('(SELECT reg_time FROM sensor_data ORDER BY reg_time DESC LIMIT 1) UNION ALL (SELECT reg_time FROM sensor_data ORDER BY reg_time ASC LIMIT 1)', function(error, results){
        if(error) return res.sendStatus(500);

        var start = results[1].reg_time || new Date();
        var end = results[0].reg_time || new Date;
        return res.send({start: start, end: end});
    });
});

app.get('/checkDelete', function(req, res){
    var start = new Date(new Date(req.query.start).toUTCString().substr(0, 25));
    var end = new Date(new Date(req.query.end).toUTCString().substr(0, 25));

    connection.query('SELECT COUNT(*) AS total FROM sensor_data WHERE reg_time BETWEEN ? AND ?',[start, end], function(error, results){
        if(error) return res.sendStatus(500);

        var startStringArray = req.query.start.split(" ");
        var startDateString = startStringArray[1] + ' ' + startStringArray[2] + ' ' + startStringArray[3] + ' ' + startStringArray[4];
        var endStringArray = req.query.end.split(" ");
        var endDateString = endStringArray[1] + ' ' + endStringArray[2] + ' ' + endStringArray[3] + ' ' + endStringArray[4];

        return res.send({dataPoints: results[0].total, start: startDateString, end: endDateString});
    });
});

app.get('/deleteData', function(req, res){
    var start = new Date(new Date(req.query.start).toUTCString().substr(0, 25));
    var end = new Date(new Date(req.query.end).toUTCString().substr(0, 25));

    connection.query('DELETE FROM sensor_data WHERE reg_time BETWEEN ? AND ?',[start, end], function(error, results){
        if(error) return res.sendStatus(500);

        return res.sendStatus(200);
    });
});

app.get('/getDataCsv', function(req, res){
    var start = new Date(new Date(req.query.start).toUTCString().substr(0, 25));
    var end = new Date(new Date(req.query.end).toUTCString().substr(0, 25));

    connection.query('SELECT * FROM sensor_data WHERE reg_time BETWEEN ? AND ?',[start, end], function(error, results){
        if(error) return res.sendStatus(500);
        
        var fields = ['temperature', 'humidity', 'ozone', 'oxygen', 'carbonDioxide', 'nitrogenDioxide', 'reg_time', 'peopleNumber' ];
        for(var i = 0; i < results.length; i++){
            var timestamp = new Date(results[i].reg_time);
            results[i].reg_time = timestamp.toString();
        }
        var csv = json2csv({ data: results, fields: fields });

        return res.send(csv);
    });
});

connection.query('SELECT * FROM sensors_settings', function(error, results){
    if(error) console.log(error);

    for(var i = 0; i < results.length; i++){
        sensorsSettings[i] = {};
        sensorsSettings[i].name = results[i].name;

        if(results[i].name === 'carbonDioxide'){
            sensorsSettings[i].lowerReaction = results[i].firstParameter;
            sensorsSettings[i].higherReaction = results[i].secondParameter;
        } else {
            sensorsSettings[i].slope = results[i].firstParameter;
            sensorsSettings[i].intercept = results[i].secondParameter;
        }
    }

    connection.query('SELECT * FROM settings', function (error, results) {
        if(error) console.log(error);

        wifi = results[0].wifi == 1;
        frequency = results[0].frequency;
        switchWifi(wifi, function(){
            http.listen(8080, function(){
                console.log('listening on *:8080');
                setInterval(function(){
                    diskspace.check('/', function (err, total, free, status){
                        if(err) console.log(err);

                        var used =  Math.round((100 - (free/total)*100) * 100) / 100
                        io.emit('memoryUsageStatus', used);
                    });
                },1000);
                repeat();
            });
        });
    });

});


function repeat() {
    sensor.read(22, 4, function(err, temperature, humidity) {
        if(err) console.log(err);

        if(firstData){
            var date = new Date();
            for(var i = 0; i < sensorsSettings.length; i++){
                switch (sensorsSettings[i].name) {
                    case 'temperature':
                        currentValues.temperature = temperature.toFixed(2) * sensorsSettings[i].slope + sensorsSettings[i].intercept;
                        break;
                    case 'humidity':
                        currentValues.humidity = humidity = humidity.toFixed(2) * sensorsSettings[i].slope + sensorsSettings[i].intercept;
                        break;
                    case 'ozone':
                        arduinoData[0] = 1/arduinoData[0];
                        currentValues.ozone = parseFloat(arduinoData[0]) * sensorsSettings[i].slope + sensorsSettings[i].intercept;
                        break;
                    case 'oxygen':
                        currentValues.oxygen = parseFloat(arduinoData[1])*100*sensorsSettings[i].slope + sensorsSettings[i].intercept;
                        break;
                    case 'carbonDioxide':
                        currentValues.carbonDioxide = calculateCO2(sensorsSettings[i].lowerReaction, sensorsSettings[i].higherReaction, parseFloat(arduinoData[2])).toFixed(2);
                        io.emit()
                        break;
                    case 'nitrogenDioxide':
                        currentValues.nitrogenDioxide = (parseFloat(arduinoData[3])/1000)*sensorsSettings[i].slope + sensorsSettings[i].intercept;
                        break;
                    default:
                        break;
                }
            }
            io.emit('currentValues', currentValues);
            connection.query('SELECT * FROM settings', function (error, results) {
                connection.query('INSERT INTO sensor_data(temperature, humidity, ozone, oxygen, carbonDioxide, nitrogenDioxide, reg_time, peopleNumber) VALUES(?,?,?,?,?,?,?,?)', 
                [currentValues.temperature, currentValues.humidity, currentValues.ozone, currentValues.oxygen, currentValues.carbonDioxide, currentValues.nitrogenDioxide, date, results[0].peopleNumber], function(error){
                    if(error) console.log(error);

                });
            });
        }
        timer = setTimeout(repeat, frequency*1000);
    });
};

function switchWifi(wifi, callback) {
    if(!wifi){
        exec('sudo service hostapd stop', function(error){
            exec('sudo service isc-dhcp-server stop', function(error){
                exec('sudo ifdown wlan0', function(error){
                    callback();
                });
            });
        });
    } else {
        exec('sudo ifup wlan0', function(error){
            exec('sudo service hostapd start', function(error){
                exec('sudo service isc-dhcp-server start', function(error){
                    callback();
                });
            });
        });
    }
}


function calculateCO2(lowerReaction, higherReaction, sensorVoltage){
    return Math.pow(10, ((sensorVoltage-lowerReaction)*(Math.log10(1000)-Math.log10(400)))/(higherReaction-lowerReaction) + Math.log10(400));
}
