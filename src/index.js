import React from 'react';
import ReactDOM from 'react-dom';
import Home from './common/home.component.jsx';
import Settings from './common/settings.component.jsx';
import Main from './main.js';
import {Router, Route, browserHistory, IndexRoute} from 'react-router';
import './index.css';
import injectTapEventPlugin from 'react-tap-event-plugin';

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

ReactDOM.render(
  <Router history={browserHistory}>
    <Route path="/" component={Main} history={browserHistory}>
      <IndexRoute component={Home} />
      <Route path="/settings" component={Settings}/>
    </Route>
  </Router>,
  document.getElementById('root')
);
