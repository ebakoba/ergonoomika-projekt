import React, { Component } from 'react';
import MemoryUsage from './elements/MemoryUsage.jsx';
import SaveData from './elements/SaveData.jsx';
import CurrentValues from './elements/CurrentValues.jsx';
import {Container} from 'react-grid-system';


class Home extends Component {

    render(){
        return (
            <Container>
                <CurrentValues>
                </CurrentValues>

                <SaveData>
                </SaveData>

                <MemoryUsage>
                </MemoryUsage>
            </Container>
        );
    }
}

export default Home