import React, { Component } from 'react';
import {TextField, Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn, RaisedButton} from 'material-ui';
import {Container, Row, Col} from 'react-grid-system';
import InfoContainer from './InfoContainer.jsx';
import io from 'socket.io-client';
let socket = io('http://'+window.location.hostname+':8080');

const styles = {
  customWidth: {
    width: 150,
  },
};

class CalibrationParameters extends Component {

    constructor(props) {
        super(props);

        var initialState = {
            sensorsSettings:[{name: 'ozone', slope: 1, intercept:0}, {name: 'humidity', slope: 1, intercept:0}, {name: 'temperature', slope: 1, intercept:0},
            {name: 'carbonDioxide', lowerReaction: 600, higherReaction:820}],
            showLinearCalibration: false,
            showLogaritmicCalibration: false,
            linearSensor: '',
            slope: 0,
            intercept: 0,
            lowerReaction: 0,
            higherReaction: 0,
            logaritmicSensor: '',
            carbonDioxideVoltage: 0,
        };

        this.state = initialState;
    }

    componentDidMount() {    
        socket.on(`sensorsSettings`, newValue => {
            this.setState({sensorsSettings: newValue});
        });
        socket.on('carbonDioxideVoltage', newValue => {
            this.setState({carbonDioxideVoltage:newValue});
        })
    }

    componentWillMount = () => {
        const _this = this;
        fetch('http://'+window.location.hostname+':8080/getSensorsSettings').then(function(response){
            return response.json();
        }).then(function(data){
            _this.setState({sensorsSettings: data.sensorsSettings});
        });
    }

    handleLinearRowSelection = (selectedRows) => {
        if(selectedRows[0] !== undefined){
            var linearSensors = this.state.sensorsSettings.filter(function(sensor){
                if(sensor.slope !== undefined) return sensor;
            });
            this.setState({showLinearCalibration:true, linearSensor:linearSensors[selectedRows[0]].name, slope: linearSensors[selectedRows[0]].slope, intercept: linearSensors[selectedRows[0]].intercept});
        } else {
            this.setState({showLinearCalibration:false});
        }
    }

    handleLogaritmicRowSelection = (selectedRows) => {
        if(selectedRows[0] !== undefined){
            var logaritmicSensors = this.state.sensorsSettings.filter(function(sensor){
                if(sensor.lowerReaction !== undefined) return sensor;
            });
            this.setState({showLogaritmicCalibration:true, logaritmicSensor:logaritmicSensors[selectedRows[0]].name, lowerReaction:logaritmicSensors[selectedRows[0]].lowerReaction, higherReaction:logaritmicSensors[selectedRows[0]].higherReaction});
        } else {
            this.setState({showLogaritmicCalibration:false});
        }
    }

    handleSlopeChange = (event, value, nextValue) => {
        this.setState({slope: value});
    }

    handleInterceptChange = (event, value) => {
        this.setState({intercept: value});
    }

    handleLowerReactionChange = (event, value) => {
        this.setState({lowerReaction: value});
    }

    handleHigherReactionChange = (event, value) => {
        this.setState({higherReaction: value});
    }

    linearSensorSettingsChange = () => {
        fetch('http://'+window.location.hostname+':8080/changeSensorsSettings?sensor=' + this.state.linearSensor +'&firstParameter=' + parseFloat(String(this.state.slope)) +'&secondParameter=' + parseFloat(String(this.state.intercept)));
    }

    logaritmicSensorSettingsChange = () => {
        fetch('http://'+window.location.hostname+':8080/changeSensorsSettings?sensor=' + this.state.logaritmicSensor +'&firstParameter=' + parseFloat(String(this.state.lowerReaction)) +'&secondParameter=' + parseFloat(String(this.state.higherReaction)));
    }

    render(){
        return (
        <InfoContainer title="Calibration">
            <Container>
                    <Row>
                        <Table onRowSelection={this.handleLinearRowSelection}>
                            <TableHeader>
                                <TableRow>
                                    <TableHeaderColumn>Sensor</TableHeaderColumn>
                                    <TableHeaderColumn>a (slope)</TableHeaderColumn>
                                    <TableHeaderColumn>b (intercept)</TableHeaderColumn>
                                </TableRow>
                            </TableHeader>
                            <TableBody>
                                {this.state.sensorsSettings.map(function(sensor, index){
                                    if (sensor.slope) return (
                                        <TableRow key={index}>
                                            <TableRowColumn>{sensor.name}</TableRowColumn>
                                            <TableRowColumn>{sensor.slope}</TableRowColumn>
                                            <TableRowColumn>{sensor.intercept}</TableRowColumn>
                                        </TableRow>)
                                    return null;
                                }, this)}
                            </TableBody>
                        </Table>
                    </Row>
                    { this.state.showLinearCalibration ? <Row>
                        <Col sm={6} md={3}>
                            <h4 style={{marginTop:44, marginBottom:0}}>
                                Sensor: <i>{this.state.linearSensor}</i>
                            </h4>
                        </Col>
                        <Col sm={6} md={3}>
                            <TextField
                                value={this.state.slope}
                                onChange={this.handleSlopeChange}
                                style={styles.customWidth}
                                type="number"
                                hintText="Insert a"
                                floatingLabelText="a (slope)"
                            />
                        </Col>
                        <Col sm={6} md={3} style={styles.customWidth}>
                            <TextField
                                value={this.state.intercept}
                                onChange={this.handleInterceptChange}
                                style={styles.customWidth}
                                type="number"
                                hintText="Insert b"
                                floatingLabelText="b (intercept)"
                            />
                        </Col>
                        <Col sm={6} md={3}>
                            <RaisedButton label="Recalibrate" primary={true} style={{marginTop:30}} onClick={this.linearSensorSettingsChange} />
                        </Col>
                    </Row> : null }
                    <br/>
                    <br/>
                    <Row>
                        <Table onRowSelection={this.handleLogaritmicRowSelection}>
                            <TableHeader>
                                <TableRow>
                                    <TableHeaderColumn>Sensor</TableHeaderColumn>
                                    <TableHeaderColumn>400ppm</TableHeaderColumn>
                                    <TableHeaderColumn>1000pm</TableHeaderColumn>
                                </TableRow>
                            </TableHeader>
                            <TableBody>
                                {this.state.sensorsSettings.map(function(sensor, index){
                                    if (sensor.lowerReaction) return (
                                    <TableRow key={index}>
                                            <TableRowColumn>{sensor.name}</TableRowColumn>
                                            <TableRowColumn>{sensor.lowerReaction + 'mV'}</TableRowColumn>
                                            <TableRowColumn>{sensor.higherReaction + 'mV'}</TableRowColumn>
                                        </TableRow>)
                                    return null;
                                }, this)}
                            </TableBody>
                        </Table>
                    </Row>
                    { this.state.showLogaritmicCalibration ? <Row>
                        <Col xs={12} style={{marginTop:16}}> 
                            Live Carbon dioxide sensor voltage: <i>{this.state.carbonDioxideVoltage + 'mV'}</i>
                        </Col>
                        <Col sm={6} md={3}>
                            <h4 style={{marginTop:44, marginBottom:0}}>
                                Sensor: <i>{this.state.logaritmicSensor}</i>
                            </h4>
                        </Col>
                        <Col sm={6} md={3}>
                            <TextField
                                value={this.state.lowerReaction}
                                onChange={this.handleLowerReactionChange}
                                style={styles.customWidth}
                                type="number"
                                hintText="Insert mV"
                                floatingLabelText="mV on 400ppm"
                            />
                        </Col>
                        <Col sm={6} md={3} style={styles.customWidth}>
                            <TextField
                                value={this.state.higherReaction}
                                onChange={this.handleHigherReactionChange}
                                style={styles.customWidth}
                                type="number"
                                hintText="Insert mV"
                                floatingLabelText="mV on 1000ppm"
                            />
                        </Col>
                        <Col sm={6} md={3}>
                            <RaisedButton label="Recalibrate" primary={true} style={{marginTop:30}} onClick={this.logaritmicSensorSettingsChange} />
                        </Col>
                    </Row> : null }
            </Container>
        </InfoContainer>
        );
    }
}

export default CalibrationParameters