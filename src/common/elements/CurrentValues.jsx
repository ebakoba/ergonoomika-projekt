import React, { Component } from 'react';
import {Checkbox} from 'material-ui';
import InfoContainer from './InfoContainer.jsx';
import io from 'socket.io-client';
import {Container, Row, Col} from 'react-grid-system';
let socket = io('http://'+window.location.hostname+':8080');


const styles = {
    valuesMargin:{
        marginTop:8,
        marginBottom:16,
    }
}

class CurrentValues extends Component {

    constructor(props) {
        super(props);

        var initialState = {
            currentValues: [],
        };
        this.state = initialState;
    }

    changeWifiStatus(newWifiValue){
        if(newWifiValue) {
            this.setState({wifiStatus: "on"});
        } else {
            this.setState({wifiStatus: "off"});
        }
    }

    componentDidMount() {    
        socket.on('currentValues', newValue => {
            this.setState({currentValues: newValue});
        });
    }

    componentWillMount = () => {
        const _this = this;
        fetch('http://'+window.location.hostname+':8080/getCurrentValues').then(function(response){
            return response.json();
        }).then(function(data){
            _this.setState({currentValues: data.currentValues});
        });
    }

    render(){
        return (
        <InfoContainer title="Current values">
            <Container>
                    <Row>
                        <Col xs={12} style={styles.valuesMargin} sm={6}>
                            Temperature: {this.state.currentValues.temperature}°C 
                        </Col>
                        <Col xs={12} style={styles.valuesMargin} sm={6}>
                            Humidity: {this.state.currentValues.humidity}%
                        </Col>
                        <Col xs={12} style={styles.valuesMargin} sm={6}>
                            Ozone: {this.state.currentValues.ozone}ppm
                        </Col>
                        <Col xs={12} style={styles.valuesMargin} sm={6}>
                            Oxygen: {this.state.currentValues.oxygen}ppm
                        </Col>
                        <Col xs={12} style={styles.valuesMargin} sm={6}>
                            Carbon dioxide: {this.state.currentValues.carbonDioxide}ppm 
                        </Col>
                        <Col xs={12} style={styles.valuesMargin} sm={6}>
                            Nitrogen dioxide: {this.state.currentValues.nitrogenDioxide}ppm 
                        </Col>
                    </Row>
            </Container>
        </InfoContainer>
        );
    }
}

export default CurrentValues