import React, { Component } from 'react';
import {SelectField, MenuItem} from 'material-ui';
import InfoContainer from './InfoContainer.jsx';
import io from 'socket.io-client';
import {Container, Row, Col} from 'react-grid-system';
let socket = io('http://'+window.location.hostname+':8080');


const secondItems = [];
for (let i = 0; i < 60; i++ ) {
    secondItems.push(<MenuItem value={i} key={i} primaryText={`${i}s`} />);
}

const minuteItems = [];
for (let i = 0; i < 60; i++ ) {
    minuteItems.push(<MenuItem value={i} key={i} primaryText={`${i}min`} />);
}

const hourItems = [];
for (let i = 0; i < 24; i++ ) {
    hourItems.push(<MenuItem value={i} key={i} primaryText={`${i}h`} />);
}

const dayItems = [];
for (let i = 0; i < 14; i++ ) {
    dayItems.push(<MenuItem value={i} key={i} primaryText={`${i}d`} />);
}

const styles = {
  customWidth: {
    width: 100,
  },
};

class SaveFrequency extends Component {

    constructor(props) {
        super(props);

        var initialState = {
            secondValue: 20,
            minuteValue: 39,
            hourValue: 2,
            dayValue: 12,
        };

        this.state = initialState;
    }

    updateFrequency = (seconds, minutes, hours, days) => {
        var frequency = (days*24*60*60) + (hours*24*60) + (minutes*60) + seconds;
        if(frequency === 0) frequency = 1;
        fetch('http://'+window.location.hostname+':8080/changeFrequency?frequency=' + frequency);
    };

    handleSeconds = (event, index, value) => {
        this.updateFrequency(value, this.state.minuteValue, this.state.hourValue, this.state.dayValue);
    }

    handleMinutes = (event, index, value) => {
        this.updateFrequency(this.state.secondValue, value, this.state.hourValue, this.state.dayValue);
    }

    handleHours = (event, index, value) => {
        this.updateFrequency(this.state.secondValue, this.state.minuteValue, value, this.state.dayValue);
    }

    handleDays = (event, index, value) => {
        this.updateFrequency(this.state.secondValue, this.state.minuteValue, this.state.hourValue, value);
    }
    
    handleFrequencyUpdate = (frequency) => {
        var dayValue = ~~(frequency / (24*60*60));
        var hourValue = ~~((frequency - dayValue*24*60*60) / (24*60));
        var minuteValue = ~~((frequency - dayValue*24*60*60 - hourValue*24*60) / 60);
        var secondValue = (frequency - dayValue*24*60*60 - hourValue*24*60 - minuteValue*60);
        this.setState({secondValue:secondValue, minuteValue:minuteValue, hourValue:hourValue, dayValue:dayValue});
    }

    componentWillMount = () => {
        const _this = this;
        fetch('http://'+window.location.hostname+':8080/getFrequency').then(function(response){
            return response.json();
        }).then(function(data){
            _this.handleFrequencyUpdate(data.frequency);
        });
    }

    componentDidMount() {    
        socket.on(`frequencyStatus`, newValue => {
            this.handleFrequencyUpdate(newValue);
        });
    }

    render(){
        return (
            <InfoContainer title="Data colletion frequency ">
             <Container>
                <Row>
                    <Col xs={6}>
                        <SelectField
                            floatingLabelText="days"
                            value={this.state.dayValue}
                            onChange={this.handleDays}
                            maxHeight={200}
                            style={styles.customWidth}
                        >
                            {dayItems}
                        </SelectField>
                        <SelectField
                            floatingLabelText="hours"                   
                            value={this.state.hourValue}
                            onChange={this.handleHours}
                            maxHeight={200}
                            style={styles.customWidth}
                        >
                            {hourItems}
                        </SelectField>
                    </Col>
                    <Col xs={6}>
                        <SelectField
                            floatingLabelText="minutes"
                            value={this.state.minuteValue}
                            onChange={this.handleMinutes}
                            maxHeight={200}
                            style={styles.customWidth}
                        >
                            {minuteItems}
                        </SelectField>
                        <SelectField
                            floatingLabelText="seconds"
                            value={this.state.secondValue}
                            onChange={this.handleSeconds}
                            maxHeight={200}
                            style={styles.customWidth}
                        >
                            {secondItems}
                        </SelectField>
                    </Col>
                </Row>
            </Container>
            </InfoContainer>
        );
    }
}

export default SaveFrequency