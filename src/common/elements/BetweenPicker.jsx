import React, { Component } from 'react';
import {DatePicker, TimePicker} from 'material-ui';
import {Col, Row, Container} from 'react-grid-system';

class BetweenPicker extends Component {

    constructor(props) {
        super(props);

        var initialState = {
            minDateTime: new Date(),
            maxDateTime: new Date(),
        };

        this.state = initialState;
    }

    componentWillMount = () => {
        const _this = this;
        fetch('http://'+window.location.hostname+':8080/getDataTimes').then(function(response){
            return response.json();
        }).then(function(data){
            var minDateTime = new Date(data.start);
            var maxDateTime = new Date(data.end);
            _this.setState({minDateTime: minDateTime, maxDateTime: maxDateTime});
        });
    }

    handleChangeMinDate = (event, date) => {
        this.setState({
            minDateTime: new Date(date.getFullYear(), date.getMonth(), date.getDate(), 
               this.state.minDateTime.getHours(), this.state.minDateTime.getMinutes(), this.state.minDateTime.getSeconds()),
        });
    };

    handleChangeMaxDate = (event, date) => {
        this.setState({
            maxDateTime: new Date(date.getFullYear(), date.getMonth(), date.getDate(), 
               this.state.maxDateTime.getHours(), this.state.maxDateTime.getMinutes(), this.state.maxDateTime.getSeconds()),
        });
    };

    handleChangeMinTime = (event, time) => {
        this.setState({
            minDateTime: new Date(this.state.minDateTime.getFullYear(), this.state.minDateTime.getMonth(), this.state.minDateTime.getDate(), 
               time.getHours(), time.getMinutes(), time.getSeconds()),
        });
    }

    handleChangeMaxTime = (event, time) => {
        this.setState({
            maxDateTime: new Date(this.state.maxDateTime.getFullYear(), this.state.maxDateTime.getMonth(), this.state.maxDateTime.getDate(), 
               time.getHours(), time.getMinutes(), time.getSeconds()),
        });
    }

    render(){
        return (
        <Container>
            <Row>
                <Col sm={6}>
                    <h4>Start time</h4>
                    <TimePicker
                    format="24hr"
                    hintText="Start Time"
                    value={this.state.minDateTime}
                    onChange={this.handleChangeMinTime}
                    />
                    <DatePicker hintText="Start Date"
                    value={this.state.minDateTime}
                    onChange={this.handleChangeMinDate}
                    maxDate={this.state.maxDate}
                    defaultDate={this.state.minDateTime} />
                </Col>
                <Col sm={6}>
                    <h4>End time</h4>
                    <TimePicker
                    format="24hr"
                    hintText="End Time"
                    value={this.state.maxDateTime}
                    onChange={this.handleChangeMaxTime}
                    />
                    <DatePicker hintText="End Date"
                    value={this.state.maxDateTime}
                    onChange={this.handleChangeMaxDate}
                    minDate={this.state.minDateTime}
                    defaultDate={this.state.maxDateTime} />
                </Col>
            </Row>
            {this.props.children}
        </Container>  
        );
    }
}

export default BetweenPicker