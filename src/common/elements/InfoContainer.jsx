import React, { Component } from 'react';
import {AppBar, Paper} from 'material-ui';
import {Col, Row} from 'react-grid-system';

const styles = {
  container: {
    paddingTop: 24,
    paddingBottom: 16,
  },
  contentPadding: {
      padding:16
  }
};

class InfoContainer extends Component {
    render(){
        return (
        <Row style={styles.container}>
            <Col xs={12}>
                <Paper zDepth={3}>
                    <AppBar
                    title={this.props.title}
                    iconElementLeft={<div></div>}/>
                    <div style={styles.contentPadding}>
                        {this.props.children}
                    </div>
                </Paper>
            </Col>
        </Row>
        );
    }
}

export default InfoContainer