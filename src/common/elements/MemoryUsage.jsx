import React, { Component } from 'react';
import {LinearProgress, RaisedButton, Dialog, FlatButton} from 'material-ui';
import BetweenPicker from './BetweenPicker.jsx';
import InfoContainer from './InfoContainer.jsx';
import io from 'socket.io-client';
let socket = io('http://'+window.location.hostname+':8080');


class MemoryUsage extends Component {

    constructor(props) {
        super(props);

        var initialState = {
            used:0,
            dialogOpen:false,
        };

        this.state = initialState;
    }

    progressColorHandler = (used) => {
        if(used < 50){
            this.setState({color: "Green"});
        } else if(used < 75){
            this.setState({color: "Yellow"});
        } else {
            this.setState({color: "Red"});
        }
    }


    handleCloseCancel = () => {
        this.setState({dialogOpen: false});
    };

    handleCloseSuccess = () => {
        this.setState({dialogOpen: false});
        fetch('http://'+window.location.hostname+':8080/deleteData?start=' + this.refs.deletePicker.state.minDateTime +'&end=' + this.refs.deletePicker.state.maxDateTime);
    };

    componentWillMount = () => {
        const _this = this;
        fetch('http://'+window.location.hostname+':8080/MemoryUsage').then(function(response){
            return response.json();
        }).then(function(data){
            _this.setState({used: data.used});
            _this.progressColorHandler(data.used);
        });
    }

    componentDidMount() {    
        socket.on(`memoryUsageStatus`, newValue => {
            this.setState({used: newValue});
            this.progressColorHandler(newValue);
        });
    }
    
    startDeleting = () => {
        const _this = this;
        fetch('http://'+window.location.hostname+':8080/checkDelete?start=' + this.refs.deletePicker.state.minDateTime +'&end=' + this.refs.deletePicker.state.maxDateTime).then(function(response){
            return response.json();
        }).then(function(data){
            _this.setState({deleteStart: data.start, deleteEnd: data.end, dataPoints: data.dataPoints});
            _this.setState({dialogOpen:true});
        });
    }

    render(){

        const actions = [
            <FlatButton
            label="Cancel"
            primary={true}
            onTouchTap={this.handleCloseCancel}
            />,
            <FlatButton
            label="Submit"
            primary={true}
            keyboardFocused={true}
            onTouchTap={this.handleCloseSuccess}
            />,
        ];

        return (

            <InfoContainer title="Memory usage">
                <p>Memory used: <b>{this.state.used}%</b></p>
                <LinearProgress mode="determinate" color={this.state.color} value={this.state.used} />
                <br/>
                <br/>
                <BetweenPicker ref="deletePicker">
                    <br />
                    <br />
                    <RaisedButton label="Delete data between dates" onTouchTap={this.startDeleting}
                    fullWidth={true} secondary={true} />
                    <Dialog
                    title="Delete check"
                        actions={actions}
                        modal={false}
                        open={this.state.dialogOpen}
                        onRequestClose={this.handleCloseCancel}
                    >
                    Do you really want to delete {this.state.dataPoints} data points starting from {this.state.deleteStart} up to {this.state.deleteEnd} ?
                    </Dialog>

                </BetweenPicker>
            </InfoContainer>
        );
    }
}

export default MemoryUsage