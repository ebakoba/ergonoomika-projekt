import React, { Component } from 'react';
import {RaisedButton, CircularProgress} from 'material-ui';
import BetweenPicker from './BetweenPicker.jsx';
import InfoContainer from './InfoContainer.jsx';
import fileDownload  from 'react-file-download';


const styles = {
  loaderHolder: {
    textAlign: 'center',
    marginTop: 16
  },
};

class SaveData extends Component {

    constructor(props) {
        super(props);

        var initialState = {
            showLoader: false
        };

        this.state = initialState;
    }

    startDownload = () => {
        this.setState({showLoader:true});
        const _this = this;
        fetch('http://'+window.location.hostname+':8080/getDataCsv?start=' + this.refs.savePicker.state.minDateTime +'&end=' + this.refs.savePicker.state.maxDateTime)
        .then(function(response){
            return response.text();
        }).then(function(data){
            _this.setState({showLoader:false});
            fileDownload(data, 'data.csv');
        });
    }

    render(){
        return (
        <InfoContainer title="Save data">
            <BetweenPicker ref="savePicker">
                <br />
                <br />
                <RaisedButton label="Save data as CSV file" onTouchTap={this.startDownload}
                fullWidth={true} primary={true} disabled={this.state.showLoader} />
                <div style={styles.loaderHolder}>
                    { this.state.showLoader ? <CircularProgress size={80} thickness={5} /> : null }
                </div>
            </BetweenPicker>
        </InfoContainer>
        );
    }
}

export default SaveData