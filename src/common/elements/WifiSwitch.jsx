import React, { Component } from 'react';
import {Checkbox} from 'material-ui';
import InfoContainer from './InfoContainer.jsx';
import io from 'socket.io-client';
import {Container, Row, Col} from 'react-grid-system';
let socket = io('http://'+window.location.hostname+':8080');

class WifiSwitch extends Component {

    constructor(props) {
        super(props);

        var initialState = {
            wifiValue: false,
            wifiStatus:'off'
        };
        this.state = initialState;
    }

    changeWifiStatus(newWifiValue){
        if(newWifiValue) {
            this.setState({wifiStatus: "on"});
        } else {
            this.setState({wifiStatus: "off"});
        }
    }

    componentDidMount() {    
        socket.on(`wifiStatus`, newValue => {
            this.setState({wifiValue: newValue});
            this.changeWifiStatus(newValue);
        });
    }

    componentWillMount = () => {
        const _this = this;
        fetch('http://'+window.location.hostname+':8080/getWifi').then(function(response){
            return response.json();
        }).then(function(data){
            _this.setState({wifiValue: data.wifi});
            _this.changeWifiStatus(data.wifi);
        });
    }

    handleWifiSwitch = (event, value) => {
        fetch('http://'+window.location.hostname+':8080/switchWifi?wifi=' + value);
    }

    render(){
        return (
        <InfoContainer title="Wifi">
            <Container>
                    <Row>
                        <Col xs={12}>
                            <Checkbox onCheck={this.handleWifiSwitch} checked={this.state.wifiValue} label={'Wifi is ' + this.state.wifiStatus}/>
                        </Col>
                    </Row>
            </Container>
        </InfoContainer>
        );
    }
}

export default WifiSwitch