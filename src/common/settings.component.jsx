import React, { Component } from 'react';
import {Container} from 'react-grid-system';
import SaveFrequency from './elements/SaveFrequency.jsx';
import WifiSwitch from './elements/WifiSwitch.jsx';
import CalibrationParameters from './elements/CalibrationParameters.jsx';

class Settings extends Component {
    render(){
        return (
            <Container>
                <SaveFrequency>
                </SaveFrequency>
                
                <WifiSwitch>
                </WifiSwitch>
                
                <CalibrationParameters>
                </CalibrationParameters>
            </Container>
        );
    }
}

export default Settings