import React, {Component} from 'react';
import {Link} from 'react-router';
import { AppBar, MenuItem, Drawer} from 'material-ui';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Settings from 'material-ui/svg-icons/action/settings';
import Home from 'material-ui/svg-icons/action/home';



const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
};

class Main extends Component {

    constructor(props) {
        super(props);
        this.state = {open: false};
    }

    handleToggle = () => this.setState({open: !this.state.open});

    handleClose = () => this.setState({open: false});

    render(){
        return(
            <MuiThemeProvider>
                <div>
                    <AppBar title="Device Panel" 
                    onLeftIconButtonTouchTap={this.handleToggle} >
                    </AppBar>
                    <Drawer
                        docked={false}
                        open={this.state.open}
                        onRequestChange={(open) => this.setState({open})}>

                        <MenuItem onTouchTap={this.handleClose} 
                            containerElement={<Link to="/" />} 
                            primaryText="Home" 
                            leftIcon={<Home />}/>
                        <MenuItem onTouchTap={this.handleClose} 
                            containerElement={<Link to="/settings" />} 
                            primaryText="Settings" 
                            leftIcon={<Settings />}/>
                    </Drawer>                  
                    <div style={styles.root}>
                        {this.props.children}
                    </div>
                </div>
            </MuiThemeProvider>
        );
    }
}

export default Main